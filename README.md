# docs-astropy4.2

PDF version of Astropy documentation.

This project is a Sphinx HTML repository for <https://astropy-doc-pdf.readthedocs.io>

&copy; 2021, Astropy BSD-3 license.
