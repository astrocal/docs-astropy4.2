.. astropy-doc-pdf documentation master file, created by
   sphinx-quickstart on Thu Apr  1 16:29:57 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Astropy Docs in PDF
===================

This site reproduced the Astropy's documentation (version 4.2 only)
as a single PDF file.

.. toctree::
   :maxdepth: 3
   :caption: Contents:
   :hidden:

   practice
   LICENSE

| Click |heresthedoc| (download size: 16.3 MiB)
| for the PDF version of Astropy-v4.2-doc_.

| Click |herestheindex| (download size: 347.2 kiB)
| for the PDF version of Astropy-v4.2-index_.

| Click |heresthemodindex| (download size: 56.8 kiB)
| for the PDF version of Astropy-v4.2-pymodindex_.

.. |nbsp| unicode:: 0xA0
   :trim:

.. _Astropy-v4.2-doc: https://docs.astropy.org/en/v4.2/
.. _Astropy-v4.2-index: https://docs.astropy.org/en/v4.2/genindex.html
.. _Astropy-v4.2-pymodindex: https://docs.astropy.org/en/v4.2/py-modindex.html

.. |heresthedoc| replace:: |nbsp| |nbsp| |nbsp| |nbsp| \>\ :download:`Astropy Doc in PDF <../pdfs/Astropy_v4.2.pdf>`\<\ |nbsp| |nbsp| |nbsp| |nbsp|
.. |herestheindex| replace:: |nbsp| |nbsp| |nbsp| |nbsp| \>\ :download:`Astropy Index <../pdfs/Astropy_v4.2_Index.pdf>`\<\ |nbsp| |nbsp| |nbsp| |nbsp|
.. |heresthemodindex| replace:: |nbsp| |nbsp| |nbsp| |nbsp| \>\ :download:`Astropy Module-Index <../pdfs/Astropy_v4.2_PyModuleIndex.pdf>`\<\ |nbsp| |nbsp| |nbsp| |nbsp|

.. Note:: All PDF documents contain no hyperlinks and are licensed under BSD-3_ by the Astropy developers.

.. _BSD-3: LICENSE.html

~~~~

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
* practice_

.. _practice: practice.html

.. role:: raw-html(raw)
   :format: html

:raw-html:`<center>~ &Omega; ~</center>`

