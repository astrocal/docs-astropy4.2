# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'astropy-doc-pdf'
copyright = '2021, Bowens'
author = 'Bowens'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
  'sphinx.ext.imgmath',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'alabaster'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static', 'css']

#html_theme_options = {
    # Disable showing the sidebar. Defaults to 'false'
#    'nosidebar': True,
#}

# Custom css
# Same effect as below.
# def setup(app):
#  app.add_css_file('theme_overrides.css')
#  app.add_css_file('math.css')

# 2021Apr03
# Apart from added path above, individual stylesheets are needed to be declared so that they are added into the generated HTML page.
html_css_files = [
  # Use to set the size of math block.
  'math.css',
  'theme_overrides.css',
]

# 2021Apr03
# [https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-extensions]
source_parsers = {'.md': 'recommonmark.parser.CommonMarkParser'}

# 2021Apr03
# [https://www.sphinx-doc.org/en/master/usage/extensions/math.html#module-sphinx.ext.imgmath]
imgmath_image_format = 'svg'
imgmath_font_size = 28