.. astropy-doc-pdf documentation master file, created by
   sphinx-quickstart on Thu Apr  1 16:29:57 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. role:: raw-html(raw)
   :format: html

ReStructuredText |br| Practice Page
===================================

.. |br| replace:: :raw-html:`<br>`
..  :trim:

This site reproduced the Astropy's documentation (version 4.2 only)
as a single PDF file.

.. Must have toctree to declare inter-relationship of html pages.

.. toctree::
   :maxdepth: 3
   :caption: Contents:

| Click  |heresthedoc|  for the PDF version of Astropy-v4.2-doc_.
| (download size: 16.3 MiB)

| Click  |herestheindex|  for the PDF version of Astropy-v4.2-index_.
| (download size: 347.2 kiB)

| Click  |heresthemodindex|  for the PDF version of Astropy-v4.2-pymodindex_.
| (download size: 56.8 kiB)

.. |nbsp| unicode:: 0xA0
   :trim:

.. _Astropy-v4.2-doc: https://docs.astropy.org/en/v4.2/
.. _Astropy-v4.2-index: https://docs.astropy.org/en/v4.2/genindex.html
.. _Astropy-v4.2-pymodindex: https://docs.astropy.org/en/v4.2/py-modindex.html

.. |heresthedoc| replace:: |nbsp| |nbsp| |nbsp| |nbsp| \>\ :download:`Astropy Doc in PDF <../pdfs/Astropy_v4.2.pdf>`\<\ |nbsp| |nbsp| |nbsp| |nbsp|
.. |herestheindex| replace:: |nbsp| |nbsp| |nbsp| |nbsp| \>\ :download:`Astropy Index <../pdfs/Astropy_v4.2_Index.pdf>`\<\ |nbsp| |nbsp| |nbsp| |nbsp|
.. |heresthemodindex| replace:: |nbsp| |nbsp| |nbsp| |nbsp| \>\ :download:`Astropy Module-Index <../pdfs/Astropy_v4.2_PyModuleIndex.pdf>`\<\ |nbsp| |nbsp| |nbsp| |nbsp|

.. note:: All hyperlinks in the PDF documents are missing.

.. attention:: No Sidebar navigation required for simple page.

.. Subtitution works with inline replacement only.

.. This is a true RST comment. I.e. any text here will not appear in HTML comment.

..
   raw:: pdf
   :file:
   :encoding: ISO-8859-1

.. :encoding: the encoding of the PDF file, if different from the
               reStructuredText document's encoding.


.. role:: raw-html(raw)
   :format: html

Unrelated issue concerning missing \ :raw-html:`&Delta;`\ T\ :sub:`i` :sup:`TT-UT1`\  info.

.. Comment-out |Delta| file::`../pdfs/Astropy_v4.2_index.pdf`

Let's do some maths too.

.. math:: \frac{ \sum_{t=0}^{N}f(t,k) }{N\times\Delta t}

Or you can do inline :raw-html:`<br>`
like so, :math:`\frac{ \sum_{t=0}^{N}f(t,k) }{N}`

How to center justify a table heading?

===== =====  ======
   Inputs     Output
-----------  ------
   A     B    A or B
===== =====  ======
False False  False
True  False  True
False  True   True
True   True   True
===== =====  ======

$$$$

Any non-alphanumberic characters repeated 4 or more times characters are <u>interpreted</u> as **transition**, i.e. an <hr> tag in *HTML*.

????

Let see how they look in *HTML*

~~~~

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

---

.. role:: raw-html(raw)
   :format: html

:raw-html:`<center>~ &Omega; ~</center>`

